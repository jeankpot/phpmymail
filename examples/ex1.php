<?php
  require_once("../class.mymail.php");

  $to = "paraquem@dominio.com";
  $from = "dequem@dominio.com";
  $subject = "Assunto do e-mail";
  $message = "Corpo do e-mail! <strong>Pode conter HTML</strong>";
  $replies = "emailparareceberresposta@dominio.com";
  $cc = "comcopiapara@dominio.com";
  $bcc = "comcopiaocultapara@dominio.com";

  /*
    Exemplo simples de envio de e-mail.
    É importante ressaltar que onde é ~array~ deve ser um array, nem que só possua uma posição.
    Os valores setados acima poderiam vir de um formulário (até um forma de array).
  */

  $mymail = new MyMail();

  $mymail->setTo(array($to)); //obrigatório

  $mymail->setFrom($from); //obrigatório

  $mymail->setSubject($subject); //obrigatório

  $mymail->setMessage($message); //obrigatório

  $mymail->setReplies(array($replies));

  $mymail->setCC(array($cc));

  $mymail->setBCC(array($bcc));

  if($mymail->sendMail()===true){
    echo "E-mail enviado com sucesso!";
  }else{
    echo "Problema ao enviar o e-mail.";
  }
  echo "Testando envio de e-mail...";
 ?>
